## Store ansible facts

Store facts under /etc/ansible/facts.d

https://docs.ansible.com/ansible/latest/user_guide/playbooks_vars_facts.html#facts-d-or-local-facts

Tested on debian 10.

## Role parameters

| name                      | type    | optionnal | default value | description                                   |
| --------------------------|---------|-----------|---------------|-----------------------------------------------|
| store_facts_file          | string  | no        |               | name of the dict entry contining our fact     |
| store_facts_vars          | string  | no        |               | value of our fact                             |
| store_facts_merge         | boolean | yes       | yes           | if yes, the existing fact file will be merged |

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/stores_fact.git
  scm: git
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - name: store_fact
      store_facts_file: 'test42'
      store_facts_vars:
        foo: 'bar'
    - name: store_fact
      store_facts_merge: no
      store_facts_file: 'not_merged'
      store_facts_vars:
        will_be_removed: 1
        test:
          a: 'a'
          c: 'c'
  post_tasks:
    - name: show how to access fact
      debug:
        msg:
          - "{{ ansible_local[test42] }}"
          - "{{ ansible_local[not_merged] }}"
```

## Tests

[tests/tests_store_facts](tests/tests_store_facts)
